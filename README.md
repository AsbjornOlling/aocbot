# AoC Bot

This is a (very quick and dirty) Matrix bot made for Advent of Code 2020.

The bot will send messages to a room, whenever somebody on a private leaderboard solves a new star.

It only works for private leaderboards.

The bot checks the leaderboard every 15 minutes, and only posts the latest new
star. So, if somebody solves two stars within the same 15-minute window, the
room is only told about the latest one.

![](scrot.png)


## Installation

### 1. Set up a new Advent of Code user

You will need to sign up a separate Advent of Code user for the bot.
This is because the website needs a session cookie, and seems to not allow
more than one concurrent session.

Sign up the bot user in a private-mode browser.

Once you have signed up a user for the bot, add it to your private leaderboard, and
extract it's session cookie. This token should be valid for about 30 days.


### 2. Get the API URL for your bot

Navigate to your private leaderboard, click "API", and then click "JSON".
Save that URL.


### 3. Create a Matrix user for your bot

You also need to create a user for the bot in your Matrix server. The bot does
implement registration itself. Once you have the user, save it's username and
password.


### 4. Populate .env file (or environment variables)

Now you can populate a .env file with the following environment variables:

- `HOMESERVER`: url of the homeserver, including schema
- `USERNAME`: localpart of the bots user id
- `PASSWORD`: password to use with the bot
- `ROOMALIAS`: room alias of room to send completion announcements in
- `AOC_URL`: URL of the JSON API
- `AOC_TOKEN`: the session cookie of your AoC bot user

There is a file named `.env.sample` which you may use as a template.
If you rename it to `.env`, the Makefile will find and use it.

### 5. Run the docker container

Running `$ make docker-run` will build and run the docker container.
