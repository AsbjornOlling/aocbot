import copy
import sys
import pathlib

newpath = str(pathlib.Path(__file__).resolve().parent.parent)
sys.path.append(newpath)
import bot

old_leaderboard = {
    "1234567": {
        "global_score": 0,
        "completion_day_level": {
            "1": {
                "2": {"get_star_ts": "1606906604"},
                "1": {"get_star_ts": "1606906133"},
            },
            "2": {
                "2": {"get_star_ts": "1607019841"},
                "1": {"get_star_ts": "1607018648"},
            },
        },
        "local_score": 46,
        "last_star_ts": "1607254699",
        "stars": 10,
        "name": None,
        "id": "1234567",
    },
    "9999999": {
        "global_score": 0,
        "completion_day_level": {},
        "local_score": 0,
        "last_star_ts": 0,
        "stars": 0,
        "id": "9999999",
        "name": "Bob the Lazy",
    },
    "123456": {
        "last_star_ts": "1607362292",
        "name": "Asbjørn Olling",
        "stars": 14,
        "id": "123456",
        "global_score": 0,
        "completion_day_level": {
            "2": {
                "1": {"get_star_ts": "1606957375"},
                "2": {"get_star_ts": "1606957964"},
            },
            "3": {
                "1": {"get_star_ts": "1606997046"},
                "2": {"get_star_ts": "1607018080"},
            },
            "1": {
                "2": {"get_star_ts": "1606848261"},
                "1": {"get_star_ts": "1606848143"},
            },
        },
        "local_score": 87,
    },
}

new_leaderboard = copy.deepcopy(old_leaderboard)
new_leaderboard["123456"]["completion_day_level"]["4"] = {
    "1": {"get_star_ts": "1606998046"},
}

new_leaderboard["1234567"]["completion_day_level"]["3"] = {
    "2": {"get_star_ts": "1607102010"},
    "1": {"get_star_ts": "1607100453"},
}

def test_changed_members_since():
    changed = bot.members_changed_between(old_leaderboard, new_leaderboard)
    assert sorted(list(changed.keys())) == sorted(["123456", "1234567"])


def test_message_for_member():
    changed = bot.members_changed_between(old_leaderboard, new_leaderboard)
    messages = [
        bot.message_for_member(m)
        for m in changed.values()
    ]
    assert sorted(messages) == sorted(['🌟 anonymous user #1234567 got star 3-2!', '🌟 Asbjørn Olling got star 3-2!'])
