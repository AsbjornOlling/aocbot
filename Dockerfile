FROM python:3.9
ENV PYTHONUNBUFFERED=1
COPY bot.py /
RUN pip install requests
CMD python /bot.py
