IMGNAME=aocbot

docker-run: docker-build
	docker run -d --env-file .env $(IMGNAME)

docker-build:
	docker build . -t $(IMGNAME)
