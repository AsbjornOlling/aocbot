import time
import random
import requests
import os


def login(homeserver, username, password):
    """ Log in to Matrix homeserver """
    # get access token
    r = requests.post(
        f"{homeserver}/_matrix/client/r0/login",
        json={
            "type": "m.login.password",
            "identifier": {"type": "m.id.user", "user": username},
            "password": password,
        },
    )
    if not r.ok:
        print(f"{r.status_code} on login")
        exit(1)
    return r.json()["access_token"]


def join_room(homeserver, access_token, roomalias):
    """ Join Matrix room using global config """
    r = requests.post(
        f"{homeserver}/_matrix/client/r0/join/{requests.utils.quote(roomalias)}",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    if not r.ok:
        print(f"{r.status_code} on join: {r.json()}")
        exit(1)
    return r.json()["room_id"]


def send_message(room_id, message):
    txn_id = random.randint(0, 1_000_000_000)
    r = requests.put(
        f"{homeserver}/_matrix/client/r0/rooms/{room_id}/send/m.room.message/{txn_id}",
        headers={"Authorization": f"Bearer {access_token}"},
        json={"msgtype": "m.text", "body": message},
    )
    if not r.ok:
        print(f"{r.status_code} while sending message: {r.json()}")
        exit(1)


def get_leaderboard():
    """ Get AoC private leaderboard using global config """
    r = requests.get(aoc_url, cookies={"session": aoc_token})
    if not r.ok:
        print(f"{r.status_code} on leaderboard get.")
        time.sleep(60)
    return r.json()


def members_changed_between(old: dict, new: dict) -> dict:
    """ Find entries that changed since last get
        member is changed if the stars list has different length
        or if member was not present before.
    """
    return {
        k: v
        for k, v in new.items()
        if (old.get(k) is None)  # if member was not on last leaderboard
        or (  # if member's stars changed
            v["completion_day_level"] != old[k]["completion_day_level"]
        )
    }


def message_for_member(member):
    """ Make message announcing the latest star of `member` """
    # get member name
    name = member.get("name")
    if name is None:
        name = f"anonymous user #{member.get('id')}"

    # get most recent star
    entry_stars = [
        (day, part, int(data["get_star_ts"]))
        for day, stars in member["completion_day_level"].items()
        for part, data in stars.items()
    ]
    if not entry_stars:
        return f"{name}"
    newest = max(entry_stars, key=lambda x: x[2])

    # format message
    message_template = "🌟 {} got star {}-{}!"
    message = message_template.format(name, newest[0], newest[1])
    return message


if __name__ == "__main__":
    homeserver = os.environ["HOMESERVER"]
    username = os.environ["USERNAME"]
    password = os.environ["PASSWORD"]
    roomalias = os.environ["ROOMALIAS"]
    aoc_url = os.environ["AOC_URL"]
    aoc_token = os.environ["AOC_TOKEN"]

    print("Getting access token...")
    access_token = login(homeserver, username, password)
    print("Got access token!")

    print("Joining room...")
    room_id = join_room(homeserver, access_token, roomalias)
    print("Joined room!")

    leaderboard = None
    while True:
        print("Trying to get leaderboard...")
        new_leaderboard = get_leaderboard()["members"]
        print("Got leaderboard!")

        if leaderboard and new_leaderboard != leaderboard:
            changed_members = members_changed_between(leaderboard, new_leaderboard)
            print(f"Changed members: {changed_members}")

            for member_id, member in changed_members.items():
                message = message_for_member(member)
                print(f"Sending message: {message}")
                send_message(room_id, message)

        # sleep 900s, as the AoC team so kindly asks us to
        time.sleep(900)
        leaderboard = new_leaderboard
